<?php

use Illuminate\Database\Seeder;

class EployeesFaker extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker\Factory::create();



      for ($i=1; $i < 1000; $i++)
      {

        DB::table('employees')->insert([
            'name'        => $faker->name,
            'address'     => $faker->address,
            'nik'         => $faker->unique(8)->randomDigitNotNull
        ]);

      }
    }
}
