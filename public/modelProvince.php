<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class Province extends Model
{
   protected $table      = "m_province";
   protected $primaryKey = "province_id";

}
