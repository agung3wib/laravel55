<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class FiLogInvoiceApBalance extends Model
{
   protected $table      = "fi_log_invoice_ap_balance";
   protected $primaryKey = "log_invoice_ap_balance_id";

}
