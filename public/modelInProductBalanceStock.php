<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class InProductBalanceStock extends Model
{
   protected $table      = "in_product_balance_stock";
   protected $primaryKey = "product_balance_stock_id";

}
