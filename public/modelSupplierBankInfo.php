<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class SupplierBankInfo extends Model
{
   protected $table      = "m_supplier_bank_info";
   protected $primaryKey = [ "supplier_id", "line_no" ];

}
