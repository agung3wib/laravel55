<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class Customer extends Model
{
   protected $table      = "m_customer";
   protected $primaryKey = "customer_id";

}
