<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class FiInvoiceArItem extends Model
{
   protected $table      = "fi_invoice_ar_item";
   protected $primaryKey = "invoice_ar_item_id";

}
