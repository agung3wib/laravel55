<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class PuPo extends Model
{
   protected $table      = "pu_po";
   protected $primaryKey = "po_id";

}
