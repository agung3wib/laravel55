<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class PuReceiveGoods extends Model
{
   protected $table      = "pu_receive_goods";
   protected $primaryKey = "receive_goods_id";

}
