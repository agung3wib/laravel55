<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class InProductBalance extends Model
{
   protected $table      = "in_product_balance";
   protected $primaryKey = "product_balance_id";

}
