<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class FiInvoiceApItem extends Model
{
   protected $table      = "fi_invoice_ap_item";
   protected $primaryKey = "invoice_ap_item_id";

}
