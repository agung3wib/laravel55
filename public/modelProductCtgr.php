<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class ProductCtgr extends Model
{
   protected $table      = "m_product_ctgr";
   protected $primaryKey = "product_ctgr_id";

}
