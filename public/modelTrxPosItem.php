<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class TrxPosItem extends Model
{
   protected $table      = "trx_pos_item";
   protected $primaryKey = "pos_item_id";

}
