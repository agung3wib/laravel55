<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class FiInvoiceApBalance extends Model
{
   protected $table      = "fi_invoice_ap_balance";
   protected $primaryKey = "invoice_ap_balance_id";

}
