<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class Warehouse extends Model
{
   protected $table      = "m_warehouse";
   protected $primaryKey = "warehouse_id";

}
