<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class Cashbank extends Model
{
   protected $table      = "m_cashbank";
   protected $primaryKey = "cashbank_id";

}
