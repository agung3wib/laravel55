<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class SupplierCp extends Model
{
   protected $table      = "m_supplier_cp";
   protected $primaryKey = [ "supplier_id", "line_no" ];

}
