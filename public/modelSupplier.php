<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class Supplier extends Model
{
   protected $table      = "m_supplier";
   protected $primaryKey = "supplier_id";

}
