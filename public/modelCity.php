<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class City extends Model
{
   protected $table      = "m_city";
   protected $primaryKey = "city_id";

}
