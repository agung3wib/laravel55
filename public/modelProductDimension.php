<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class ProductDimension extends Model
{
   protected $table      = "m_product_dimension";
   protected $primaryKey = "m_product_dimension_id";

}
