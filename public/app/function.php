<?php

function zero_magic($number, $ln)
{
  $k = $ln - strlen($number);
  if($k>0)
  {
    $str = "";
    for ($i=0; $i < $k; $i++) {
      $str .= "0";
    }
    $str .= $number;
    return $str;
  }
  else
    return $number;
}

function underScoreToUcWords($text)
{
    $ar = explode("_", $text);
    $r = "";
    foreach ($ar as $v) {
      $r .= ucfirst($v);
    }
    return $r;
}
