<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class PuPrItem extends Model
{
   protected $table      = "pu_pr_item";
   protected $primaryKey = "pr_item_id";

}
