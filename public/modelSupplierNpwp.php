<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class SupplierNpwp extends Model
{
   protected $table      = "m_supplier_npwp";
   protected $primaryKey = [ "supplier_id", "line_no" ];

}
