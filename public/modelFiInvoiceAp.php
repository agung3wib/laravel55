<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class FiInvoiceAp extends Model
{
   protected $table      = "fi_invoice_ap";
   protected $primaryKey = "invoice_ap_id";

}
