<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class FiInvoiceApBalance extends Model
{
    CONST TABLE_NAME = "fi_invoice_ap_balance";
    protected $table      = "fi_invoice_ap_balance";
    protected $primaryKey = "invoice_ap_balance_id";
    public $timestamps = false;

}
