<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class SellPriceLevel extends Model
{
    CONST TABLE_NAME = "m_sell_price_level";
    protected $table      = "m_sell_price_level";
    protected $primaryKey = "sell_price_level_id";
    public $timestamps = false;

}
