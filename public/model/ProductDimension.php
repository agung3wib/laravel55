<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class ProductDimension extends Model
{
    CONST TABLE_NAME = "m_product_dimension";
    protected $table      = "m_product_dimension";
    protected $primaryKey = "m_product_dimension_id";
    public $timestamps = false;

}
