<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class FiInvoiceArBalance extends Model
{
    CONST TABLE_NAME = "fi_invoice_ar_balance";
    protected $table      = "fi_invoice_ar_balance";
    protected $primaryKey = "invoice_ar_balance_id";
    public $timestamps = false;

}
