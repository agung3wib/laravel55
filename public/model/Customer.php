<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    CONST TABLE_NAME = "m_customer";
    protected $table      = "m_customer";
    protected $primaryKey = "customer_id";
    public $timestamps = false;

}
