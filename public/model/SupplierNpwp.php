<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class SupplierNpwp extends Model
{
    CONST TABLE_NAME = "m_supplier_npwp";
    protected $table      = "m_supplier_npwp";
    protected $primaryKey = [ "supplier_id", "line_no" ];
    public $timestamps = false;

}
