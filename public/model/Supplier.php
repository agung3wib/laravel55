<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    CONST TABLE_NAME = "m_supplier";
    protected $table      = "m_supplier";
    protected $primaryKey = "supplier_id";
    public $timestamps = false;

}
