<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class RecordOwner extends Model
{
    CONST TABLE_NAME = "m_record_owner";
    protected $table      = "m_record_owner";
    protected $primaryKey = "record_owner_id";
    public $timestamps = false;

}
