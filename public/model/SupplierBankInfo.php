<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class SupplierBankInfo extends Model
{
    CONST TABLE_NAME = "m_supplier_bank_info";
    protected $table      = "m_supplier_bank_info";
    protected $primaryKey = [ "supplier_id", "line_no" ];
    public $timestamps = false;

}
