<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class IfInPuPr extends Model
{
    CONST TABLE_NAME = "if_in_pu_pr";
    protected $table      = "if_in_pu_pr";
    
    public $timestamps = false;

}
