<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class SubDistrict extends Model
{
    CONST TABLE_NAME = "m_sub_district";
    protected $table      = "m_sub_district";
    
    public $timestamps = false;

}
