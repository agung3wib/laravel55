<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class FiInvoiceAp extends Model
{
    CONST TABLE_NAME = "fi_invoice_ap";
    protected $table      = "fi_invoice_ap";
    protected $primaryKey = "invoice_ap_id";
    public $timestamps = false;

}
