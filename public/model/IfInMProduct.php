<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class IfInMProduct extends Model
{
    CONST TABLE_NAME = "if_in_m_product";
    protected $table      = "if_in_m_product";
    
    public $timestamps = false;

}
