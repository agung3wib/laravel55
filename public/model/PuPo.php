<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class PuPo extends Model
{
    CONST TABLE_NAME = "pu_po";
    protected $table      = "pu_po";
    protected $primaryKey = "po_id";
    public $timestamps = false;

}
