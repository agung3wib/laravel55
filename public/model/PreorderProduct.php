<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class PreorderProduct extends Model
{
    CONST TABLE_NAME = "m_preorder_product";
    protected $table      = "m_preorder_product";
    protected $primaryKey = "preorder_product_id";
    public $timestamps = false;

}
