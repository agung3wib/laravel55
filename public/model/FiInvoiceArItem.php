<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class FiInvoiceArItem extends Model
{
    CONST TABLE_NAME = "fi_invoice_ar_item";
    protected $table      = "fi_invoice_ar_item";
    protected $primaryKey = "invoice_ar_item_id";
    public $timestamps = false;

}
