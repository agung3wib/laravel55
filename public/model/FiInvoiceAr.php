<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class FiInvoiceAr extends Model
{
    CONST TABLE_NAME = "fi_invoice_ar";
    protected $table      = "fi_invoice_ar";
    protected $primaryKey = "invoice_ar_id";
    public $timestamps = false;

}
