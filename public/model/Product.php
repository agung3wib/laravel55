<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    CONST TABLE_NAME = "m_product";
    protected $table      = "m_product";
    protected $primaryKey = "product_id";
    public $timestamps = false;

}
