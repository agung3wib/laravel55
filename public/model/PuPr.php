<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class PuPr extends Model
{
    CONST TABLE_NAME = "pu_pr";
    protected $table      = "pu_pr";
    
    public $timestamps = false;

}
