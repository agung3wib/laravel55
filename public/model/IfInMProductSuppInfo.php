<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class IfInMProductSuppInfo extends Model
{
    CONST TABLE_NAME = "if_in_m_product_supp_info";
    protected $table      = "if_in_m_product_supp_info";
    
    public $timestamps = false;

}
