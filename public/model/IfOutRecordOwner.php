<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class IfOutRecordOwner extends Model
{
    CONST TABLE_NAME = "if_out_record_owner";
    protected $table      = "if_out_record_owner";
    
    public $timestamps = false;

}
