<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class TrxPos extends Model
{
    CONST TABLE_NAME = "trx_pos";
    protected $table      = "trx_pos";
    protected $primaryKey = "pos_id";
    public $timestamps = false;

}
