<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    CONST TABLE_NAME = "m_province";
    protected $table      = "m_province";
    protected $primaryKey = "province_id";
    public $timestamps = false;

}
