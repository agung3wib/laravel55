<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class IfInPreorderProduct extends Model
{
    CONST TABLE_NAME = "if_in_preorder_product";
    protected $table      = "if_in_preorder_product";
    
    public $timestamps = false;

}
