<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class FiInvoiceApItem extends Model
{
    CONST TABLE_NAME = "fi_invoice_ap_item";
    protected $table      = "fi_invoice_ap_item";
    protected $primaryKey = "invoice_ap_item_id";
    public $timestamps = false;

}
