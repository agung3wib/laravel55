<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    CONST TABLE_NAME = "m_tax";
    protected $table      = "m_tax";
    
    public $timestamps = false;

}
