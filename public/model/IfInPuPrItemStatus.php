<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class IfInPuPrItemStatus extends Model
{
    CONST TABLE_NAME = "if_in_pu_pr_item_status";
    protected $table      = "if_in_pu_pr_item_status";
    
    public $timestamps = false;

}
