<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class Salesmen extends Model
{
    CONST TABLE_NAME = "m_salesmen";
    protected $table      = "m_salesmen";
    protected $primaryKey = "salesmen_id";
    public $timestamps = false;

}
