<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class TrxPosItem extends Model
{
    CONST TABLE_NAME = "trx_pos_item";
    protected $table      = "trx_pos_item";
    protected $primaryKey = "pos_item_id";
    public $timestamps = false;

}
