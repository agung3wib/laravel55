<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class IfInPuPo extends Model
{
    CONST TABLE_NAME = "if_in_pu_po";
    protected $table      = "if_in_pu_po";
    
    public $timestamps = false;

}
