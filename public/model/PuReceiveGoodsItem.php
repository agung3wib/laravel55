<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class PuReceiveGoodsItem extends Model
{
    CONST TABLE_NAME = "pu_receive_goods_item";
    protected $table      = "pu_receive_goods_item";
    protected $primaryKey = "receive_goods_item_id";
    public $timestamps = false;

}
