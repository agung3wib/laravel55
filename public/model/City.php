<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    CONST TABLE_NAME = "m_city";
    protected $table      = "m_city";
    protected $primaryKey = "city_id";
    public $timestamps = false;

}
