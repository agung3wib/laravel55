<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    CONST TABLE_NAME = "m_district";
    protected $table      = "m_district";
    
    public $timestamps = false;

}
