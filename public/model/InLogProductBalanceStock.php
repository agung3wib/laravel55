<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class InLogProductBalanceStock extends Model
{
    CONST TABLE_NAME = "in_log_product_balance_stock";
    protected $table      = "in_log_product_balance_stock";
    protected $primaryKey = "log_product_balance_stock_id";
    public $timestamps = false;

}
