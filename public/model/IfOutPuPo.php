<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class IfOutPuPo extends Model
{
    CONST TABLE_NAME = "if_out_pu_po";
    protected $table      = "if_out_pu_po";
    
    public $timestamps = false;

}
