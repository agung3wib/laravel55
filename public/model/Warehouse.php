<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    CONST TABLE_NAME = "m_warehouse";
    protected $table      = "m_warehouse";
    protected $primaryKey = "warehouse_id";
    public $timestamps = false;

}
