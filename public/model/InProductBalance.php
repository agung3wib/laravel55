<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class InProductBalance extends Model
{
    CONST TABLE_NAME = "in_product_balance";
    protected $table      = "in_product_balance";
    protected $primaryKey = "product_balance_id";
    public $timestamps = false;

}
