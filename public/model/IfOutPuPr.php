<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class IfOutPuPr extends Model
{
    CONST TABLE_NAME = "if_out_pu_pr";
    protected $table      = "if_out_pu_pr";
    
    public $timestamps = false;

}
