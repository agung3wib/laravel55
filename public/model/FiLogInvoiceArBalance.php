<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class FiLogInvoiceArBalance extends Model
{
    CONST TABLE_NAME = "fi_log_invoice_ar_balance";
    protected $table      = "fi_log_invoice_ar_balance";
    protected $primaryKey = "log_invoice_ar_balance_id";
    public $timestamps = false;

}
