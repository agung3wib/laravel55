<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class FiLogInvoiceApBalance extends Model
{
    CONST TABLE_NAME = "fi_log_invoice_ap_balance";
    protected $table      = "fi_log_invoice_ap_balance";
    protected $primaryKey = "log_invoice_ap_balance_id";
    public $timestamps = false;

}
