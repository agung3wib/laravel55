<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class PuPrItem extends Model
{
    CONST TABLE_NAME = "pu_pr_item";
    protected $table      = "pu_pr_item";
    protected $primaryKey = "pr_item_id";
    public $timestamps = false;

}
