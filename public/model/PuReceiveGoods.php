<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class PuReceiveGoods extends Model
{
    CONST TABLE_NAME = "pu_receive_goods";
    protected $table      = "pu_receive_goods";
    protected $primaryKey = "receive_goods_id";
    public $timestamps = false;

}
