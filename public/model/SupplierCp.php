<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class SupplierCp extends Model
{
    CONST TABLE_NAME = "m_supplier_cp";
    protected $table      = "m_supplier_cp";
    protected $primaryKey = [ "supplier_id", "line_no" ];
    public $timestamps = false;

}
