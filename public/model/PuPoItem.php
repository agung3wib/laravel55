<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class PuPoItem extends Model
{
    CONST TABLE_NAME = "pu_po_item";
    protected $table      = "pu_po_item";
    protected $primaryKey = "po_item_id";
    public $timestamps = false;

}
