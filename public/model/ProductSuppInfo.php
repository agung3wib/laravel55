<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class ProductSuppInfo extends Model
{
    CONST TABLE_NAME = "m_product_supp_info";
    protected $table      = "m_product_supp_info";
    protected $primaryKey = "m_product_supp_info_id";
    public $timestamps = false;

}
