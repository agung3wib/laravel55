<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class Cashbank extends Model
{
    CONST TABLE_NAME = "m_cashbank";
    protected $table      = "m_cashbank";
    protected $primaryKey = "cashbank_id";
    public $timestamps = false;

}
