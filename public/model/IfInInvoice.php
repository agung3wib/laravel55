<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class IfInInvoice extends Model
{
    CONST TABLE_NAME = "if_in_invoice";
    protected $table      = "if_in_invoice";
    
    public $timestamps = false;

}
