<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class IfInMProductCtgr extends Model
{
    CONST TABLE_NAME = "if_in_m_product_ctgr";
    protected $table      = "if_in_m_product_ctgr";
    
    public $timestamps = false;

}
