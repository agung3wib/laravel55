<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class TrxPosPayment extends Model
{
    CONST TABLE_NAME = "trx_pos_payment";
    protected $table      = "trx_pos_payment";
    protected $primaryKey = "pos_payment_id";
    public $timestamps = false;

}
