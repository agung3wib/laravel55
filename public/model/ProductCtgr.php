<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class ProductCtgr extends Model
{
    CONST TABLE_NAME = "m_product_ctgr";
    protected $table      = "m_product_ctgr";
    protected $primaryKey = "product_ctgr_id";
    public $timestamps = false;

}
