<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

class InProductBalanceStock extends Model
{
    CONST TABLE_NAME = "in_product_balance_stock";
    protected $table      = "in_product_balance_stock";
    protected $primaryKey = "product_balance_stock_id";
    public $timestamps = false;

}
