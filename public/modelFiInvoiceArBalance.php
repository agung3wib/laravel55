<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class FiInvoiceArBalance extends Model
{
   protected $table      = "fi_invoice_ar_balance";
   protected $primaryKey = "invoice_ar_balance_id";

}
