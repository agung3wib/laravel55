CREATE SEQUENCE in_product_balance_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE in_product_balance (
	product_balance_id                       bigint NOT NULL DEFAULT nextval('in_product_balance_seq'),
	record_owner_id                          bigint,
	product_id                               bigint,
	serial_number                            character varying(100),
	lot_number                               character varying(100),
	expired_date                             character varying(8),
	year_made                                character varying(4),
	version                                  bigint,
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT in_product_balance_pkey PRIMARY KEY(product_balance_id)
);

CREATE UNIQUE INDEX idx_in_product_balance_01
	ON in_product_balance
	USING btree
	(product_id, serial_number, lot_number);

