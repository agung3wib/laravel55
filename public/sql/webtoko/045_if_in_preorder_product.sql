CREATE TABLE if_in_preorder_product (
	session_uuid                             character varying(100),
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	record_owner_id                          bigint,
	valid_until_date                         character varying(8),
	curr_code_supplier_price                 character varying(3),
	supplier_code                            character varying(50),
	supplier_product_code                    character varying(50),
	min_qty                                  bigint,
	max_qty                                  bigint,
	supplier_price                           numeric,
	curr_code_estimate_price                 character varying(3),
	estimate_price                           numeric,
	flg_process                              character varying(1),
	product_code                             character varying(50)
);

