CREATE SEQUENCE m_product_ctgr_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE m_product_ctgr (
	product_ctgr_id                          bigint NOT NULL DEFAULT nextval('m_product_ctgr_seq'),
	record_owner_id                          bigint,
	product_ctgr_code                        character varying(50),
	product_ctgr_parent_id                   bigint,
	product_ctgr_name                        character varying(100),
	version                                  bigint,
	active                                   character varying(1),
	active_datetime                          character varying(14),
	non_active_datetime                      character varying(14),
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT m_product_ctgr_pkey PRIMARY KEY(product_ctgr_id)
);

CREATE UNIQUE INDEX idx_m_product_ctgr_01
	ON m_product_ctgr
	USING btree
	(record_owner_id, product_ctgr_code);

