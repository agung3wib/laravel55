CREATE TABLE if_in_invoice (
	session_uuid                             character varying(100),
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	record_owner_id                          bigint,
	invoice_uuid                             character varying(100),
	doc_type_id                              bigint,
	doc_no                                   character varying (50),
	doc_date                                 character varying (8),
	ext_doc_no                               character varying (50),
	ext_doc_date                             character varying (8),
	remark                                   character varying(100),
	invoice_amount                           numeric,
	allocation_to_invoice_doc_type_id        bigint,
	allocation_to_invoice_doc_no             character varying (50),
	flg_process                              character varying(1)
);

