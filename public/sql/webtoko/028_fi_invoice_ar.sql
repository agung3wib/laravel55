CREATE SEQUENCE fi_invoice_ar_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE fi_invoice_ar (
	invoice_ar_id                            bigint NOT NULL DEFAULT nextval('fi_invoice_ar_seq'),
	record_owner_id                          bigint,
	doc_type_id                              bigint,
	doc_no                                   character varying(50),
	doc_date                                 character varying(8),
	customer_id                              bigint,
	ext_doc_no                               character varying(50),
	ext_doc_date                             character varying(8),
	ref_doc_type_id                          bigint,
	ref_id                                   bigint,
	invoice_amount                           numeric,
	add_amount                               numeric,
	total_amount                             numeric,
	remark                                   text,
	status_doc                               character varying (1),
	version                                  bigint,
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT fi_invoice_ar_pkey PRIMARY KEY(invoice_ar_id)
);

CREATE UNIQUE INDEX idx_fi_invoice_ar_01
	ON fi_invoice_ar
	USING btree
	(record_owner_id, doc_type_id, doc_no, doc_date);

