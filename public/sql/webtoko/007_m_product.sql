CREATE SEQUENCE m_product_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE m_product (
	product_id                               bigint NOT NULL DEFAULT nextval('m_product_seq'),
	record_owner_id                          bigint,
	product_code                             character varying(50),
	product_name                             character varying(100),
	product_ctgr_id                          bigint,
	product_sub_ctgr_id                      bigint,
	part_no                                  character varying(50),
	brand_name                               character varying(100),
	color                                    character varying(50),
	style_product                            character varying(50),
	class_product                            character varying(50),
	mininum_stock_level                      bigint,
	image_url                                text,
	version                                  bigint,
	active                                   character varying(1),
	active_datetime                          character varying(14),
	non_active_datetime                      character varying(14),
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT m_product_pkey PRIMARY KEY(product_id)
);

CREATE UNIQUE INDEX idx_m_product_01
	ON m_product
	USING btree
	(record_owner_id, product_code);

