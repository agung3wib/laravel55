CREATE SEQUENCE m_product_dimension_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE m_product_dimension (
	m_product_dimension_id                   bigint NOT NULL DEFAULT nextval('m_product_dimension_seq'),
	product_id                               bigint,
	weight                                   bigint,
	length                                   bigint,
	height                                   bigint,
	width                                    bigint,
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT m_product_dimension_pkey PRIMARY KEY(m_product_dimension_id)
);

