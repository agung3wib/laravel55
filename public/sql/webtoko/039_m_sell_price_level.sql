CREATE SEQUENCE m_sell_price_level_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE m_sell_price_level (
	sell_price_level_id                      bigint NOT NULL DEFAULT nextval('m_sell_price_level_seq'),
	record_owner_id                          bigint,
	product_id                               bigint,
	price1                                   numeric,
	price2                                   numeric,
	price3                                   numeric,
	price4                                   numeric,
	price5                                   numeric,
	qty1                                     bigint,
	qty2                                     bigint,
	qty3                                     bigint,
	qty4                                     bigint,
	qty5                                     bigint,
	start_date                               character varying(8),
	end_date                                 character varying(8),
	version                                  bigint,
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT m_sell_price_level_pkey PRIMARY KEY(sell_price_level_id)
);

