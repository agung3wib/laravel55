CREATE SEQUENCE trx_pos_item_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE trx_pos_item (
	pos_item_id                              bigint NOT NULL DEFAULT nextval('trx_pos_item_seq'),
	pos_id                                   bigint,
	line_no                                  bigint,
	product_id                               bigint,
	qty                                      bigint,
	price_level_id                           bigint,
	unit_sell_price                          numeric,
	item_discount_amount                     numeric,
	item_amount                              numeric,
	item_amount_after_discount               numeric,
	item_remark                              text,
	version                                  bigint,
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT trx_pos_item_pkey PRIMARY KEY(pos_item_id)
);

CREATE UNIQUE INDEX idx_trx_pos_item_01
	ON trx_pos_item
	USING btree
	(pos_id, line_no);

