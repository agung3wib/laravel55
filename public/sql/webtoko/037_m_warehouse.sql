CREATE SEQUENCE m_warehouse_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE m_warehouse (
	warehouse_id                             bigint NOT NULL DEFAULT nextval('m_warehouse_seq'),
	record_owner_id                          bigint,
	warehouse_code                           character varying(50),
	warehouse_name                           character varying(50),
	version                                  bigint,
	active                                   character varying(1),
	active_datetime                          character varying(14),
	non_active_datetime                      character varying(14),
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT m_warehouse_pkey PRIMARY KEY(warehouse_id)
);

