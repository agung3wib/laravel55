CREATE TABLE m_supplier_cp (
	supplier_id                              bigint,
	line_no                                  bigint,
	cp_name                                  character varying(100),
	dept_name                                character varying(100),
	phone                                    character varying(100),
	email                                    character varying(100),
	version                                  bigint,
	active                                   character varying(1),
	active_datetime                          character varying(14),
	non_active_datetime                      character varying(14),
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT m_supplier_cp_pkey PRIMARY KEY(supplier_id, line_no)
);

