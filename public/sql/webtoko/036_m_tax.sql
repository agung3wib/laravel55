CREATE TABLE m_tax (
	tax_id                                   bigint,
	tax_code                                 character varying(50),
	tax_percentage                           numeric
);

