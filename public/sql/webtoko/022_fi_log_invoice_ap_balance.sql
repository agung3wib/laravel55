CREATE SEQUENCE fi_log_invoice_ap_balance_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE fi_log_invoice_ap_balance (
	log_invoice_ap_balance_id                bigint NOT NULL DEFAULT nextval('fi_log_invoice_ap_balance_seq'),
	invoice_ap_balance_id                    bigint,
	record_owner_id                          bigint,
	doc_type_id                              bigint,
	ref_id                                   bigint,
	ref_doc_no                               character varying (50),
	ref_doc_date                             character varying (8),
	amount                                   numeric,
	version                                  bigint,
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT fi_log_invoice_ap_balance_pkey PRIMARY KEY(log_invoice_ap_balance_id)
);

