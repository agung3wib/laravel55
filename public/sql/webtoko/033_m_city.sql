CREATE SEQUENCE m_city_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE m_city (
	city_id                                  bigint NOT NULL DEFAULT nextval('m_city_seq'),
	city_name                                bigint,
	province_id                              bigint,
	CONSTRAINT m_city_pkey PRIMARY KEY(city_id)
);

