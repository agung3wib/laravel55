CREATE TABLE if_in_pu_po (
	session_uuid                             character varying(100),
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	record_owner_id                          bigint,
	doc_type_id                              bigint,
	doc_no                                   character varying (50),
	doc_date                                 character varying (8),
	ext_doc_no                               character varying (50),
	ext_doc_date                             character varying (8),
	supplier_id                              bigint,
	supplier_code                            character varying(50),
	ref_doc_type_id                          bigint,
	ref_id                                   bigint,
	remark                                   text,
	curr_code                                character varying (3),
	status_doc                               character varying (1),
	po_create_datetime                       character varying(14),
	po_create_username                       character varying(50),
	po_update_datetime                       character varying(14),
	po_update_username                       character varying(50),
	product_id                               bigint,
	product_code                             character varying(50),
	qty                                      bigint,
	unit_price                               numeric,
	flg_include_tax                          character varying (1),
	tax_id                                   bigint,
	tax_percentage                           numeric,
	item_amount_discount                     numeric,
	item_amount_gross                        numeric,
	item_amount_nett                         numeric,
	item_amount_tax                          numeric,
	item_remark                              text,
	flg_process                              character varying(1)
);

CREATE UNIQUE INDEX idx_if_in_pu_po_01
	ON if_in_pu_po
	USING btree
	(product_id);

