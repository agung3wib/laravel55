CREATE SEQUENCE fi_invoice_ap_item_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE fi_invoice_ap_item (
	invoice_ap_item_id                       bigint NOT NULL DEFAULT nextval('fi_invoice_ap_item_seq'),
	invoice_ap_id                            bigint,
	invoice_item_type                        character varying(50),
	line_no                                  bigint,
	item_description                         character varying(100),
	item_amount                              numeric,
	version                                  bigint,
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT fi_invoice_ap_item_pkey PRIMARY KEY(invoice_ap_item_id)
);

CREATE UNIQUE INDEX idx_fi_invoice_ap_item_01
	ON fi_invoice_ap_item
	USING btree
	(invoice_ap_id, line_no);

