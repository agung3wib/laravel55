CREATE SEQUENCE m_salesmen_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE m_salesmen (
	salesmen_id                              bigint NOT NULL DEFAULT nextval('m_salesmen_seq'),
	record_owner_id                          bigint,
	short_name                               character varying (50),
	full_name                                character varying (50),
	phone                                    character varying (50),
	remark                                   character varying (100),
	version                                  bigint,
	active                                   character varying(1),
	active_datetime                          character varying(14),
	non_active_datetime                      character varying(14),
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT m_salesmen_pkey PRIMARY KEY(salesmen_id)
);

CREATE UNIQUE INDEX idx_m_salesmen_01
	ON m_salesmen
	USING btree
	(record_owner_id, short_name);

