CREATE SEQUENCE trx_pos_payment_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE trx_pos_payment (
	pos_payment_id                           bigint NOT NULL DEFAULT nextval('trx_pos_payment_seq'),
	pos_id                                   bigint,
	line_no                                  bigint,
	payment_type                             character varying (50),
	curr_code                                character varying (3),
	payment_ref_no                           character varying (50),
	payment_amount                           numeric,
	payment_remark                           text,
	version                                  bigint,
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT trx_pos_payment_pkey PRIMARY KEY(pos_payment_id)
);

CREATE UNIQUE INDEX idx_trx_pos_payment_01
	ON trx_pos_payment
	USING btree
	(pos_id, line_no);

