CREATE SEQUENCE m_product_supp_info_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE m_product_supp_info (
	m_product_supp_info_id                   bigint NOT NULL DEFAULT nextval('m_product_supp_info_seq'),
	supplier_id                              bigint,
	product_id                               bigint,
	supplier_product_code                    character varying(50),
	supplier_product_name                    character varying(100),
	supplier_price                           character varying(50),
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	active_date                              character varying(8),
	CONSTRAINT m_product_supp_info_pkey PRIMARY KEY(m_product_supp_info_id)
);

CREATE UNIQUE INDEX idx_m_product_supp_info_01
	ON m_product_supp_info
	USING btree
	(supplier_id, product_id);

