CREATE TABLE m_supplier_npwp (
	supplier_id                              bigint,
	line_no                                  bigint,
	no_npwp                                  character varying(16),
	npwp_date                                character varying(8),
	flg_pkp                                  character varying(1),
	pkp_date                                 character varying(8),
	version                                  bigint,
	active                                   character varying(1),
	active_datetime                          character varying(14),
	non_active_datetime                      character varying(14),
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT m_supplier_npwp_pkey PRIMARY KEY(supplier_id, line_no)
);

