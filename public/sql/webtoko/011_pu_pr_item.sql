CREATE SEQUENCE pu_pr_item_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE pu_pr_item (
	pr_item_id                               bigint NOT NULL DEFAULT nextval('pu_pr_item_seq'),
	pr_id                                    bigint,
	product_id                               bigint,
	qty_request                              bigint,
	qty_quotation                            bigint,
	default_unit_price                       numeric,
	bargain_unit_price                       numeric,
	flg_include_tax                          character varying(1),
	tax_id                                   bigint,
	tax_percentage                           numeric,
	item_amount                              numeric,
	version                                  bigint,
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT pu_pr_item_pkey PRIMARY KEY(pr_item_id)
);

