CREATE SEQUENCE pu_pr_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE pu_pr (
	pr_id                                    bigint NOT NULL DEFAULT nextval('pu_pr_seq'),
	record_owner_id                          bigint,
	doc_type_id                              bigint,
	doc_no                                   character varying (50),
	doc_date                                 character varying (8),
	ext_doc_no                               character varying (50),
	ext_doc_date                             character varying (8),
	supplier_id                              bigint,
	remark                                   text,
	curr_code                                character varying (3),
	status_doc                               character varying (1),
	version                                  bigint,
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	flg_from_preorder                        character varying (1)
);

