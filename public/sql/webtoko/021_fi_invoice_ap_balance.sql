CREATE SEQUENCE fi_invoice_ap_balance_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE fi_invoice_ap_balance (
	invoice_ap_balance_id                    bigint NOT NULL DEFAULT nextval('fi_invoice_ap_balance_seq'),
	record_owner_id                          bigint,
	invoice_ap_id                            bigint,
	supplier_id                              bigint,
	invoice_amount                           numeric,
	payment_amount                           numeric,
	flg_paid                                 character varying(1),
	version                                  bigint,
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT fi_invoice_ap_balance_pkey PRIMARY KEY(invoice_ap_balance_id)
);

CREATE UNIQUE INDEX idx_fi_invoice_ap_balance_0v
	ON fi_invoice_ap_balance
	USING btree
	(invoice_ap_id);

