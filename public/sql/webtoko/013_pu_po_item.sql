CREATE SEQUENCE pu_po_item_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE pu_po_item (
	po_item_id                               bigint NOT NULL DEFAULT nextval('pu_po_item_seq'),
	po_id                                    bigint,
	product_id                               bigint,
	qty                                      bigint,
	unit_price                               numeric,
	flg_include_tax                          character varying (1),
	tax_id                                   bigint,
	tax_percentage                           numeric,
	item_amount_discount                     numeric,
	item_amount_gross                        numeric,
	item_amount_nett                         numeric,
	item_amount_tax                          numeric,
	item_remark                              text,
	version                                  bigint,
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT pu_po_item_pkey PRIMARY KEY(po_item_id)
);

CREATE UNIQUE INDEX idx_pu_po_item_01
	ON pu_po_item
	USING btree
	(po_id, product_id);

