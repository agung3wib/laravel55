CREATE SEQUENCE in_product_balance_stock_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE in_product_balance_stock (
	product_balance_stock_id                 bigint NOT NULL DEFAULT nextval('in_product_balance_stock_seq'),
	record_owner_id                          bigint,
	product_balance_id                       bigint,
	product_id                               bigint,
	warehouse_id                             bigint,
	product_status                           character varying(50),
	qty                                      bigint,
	version                                  bigint,
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT in_product_balance_stock_pkey PRIMARY KEY(product_balance_stock_id)
);

CREATE UNIQUE INDEX idx_in_product_balance_stock_01
	ON in_product_balance_stock
	USING btree
	(record_owner_id, product_balance_id, product_id, warehouse_id, product_status);

