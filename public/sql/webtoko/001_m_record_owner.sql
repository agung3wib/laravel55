CREATE TABLE m_record_owner (
	record_owner_id                          bigint,
	ou_company                               character varying(100),
	ou_branch                                character varying(100),
	ou_sub_branch                            character varying(100),
	warehouse                                character varying(100),
	record_owner_code                        character varying(50),
	email                                    character varying(100),
	toko_id_sas                              character varying(50),
	current_status_toko                      character varying(50),
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	version                                  bigint,
	CONSTRAINT m_record_owner_pkey PRIMARY KEY(record_owner_id)
);

