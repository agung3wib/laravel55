CREATE SEQUENCE m_customer_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE m_customer (
	customer_id                              bigint NOT NULL DEFAULT nextval('m_customer_seq'),
	record_owner_id                          bigint,
	customer_code                            character varying(50),
	customer_name                            character varying(100),
	price_level_id                           bigint,
	phone                                    character varying(50),
	address1                                 character varying(100),
	address2                                 character varying(100),
	province                                 bigint,
	city                                     bigint,
	npwp                                     character varying(16),
	npwp_date                                character varying(8),
	cp_name                                  character varying(50),
	cp_email                                 character varying(50),
	cp_phone                                 character varying(50),
	version                                  bigint,
	active                                   character varying(1),
	active_datetime                          character varying(14),
	non_active_datetime                      character varying(14),
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT m_customer_pkey PRIMARY KEY(customer_id)
);

CREATE UNIQUE INDEX idx_m_customer_01
	ON m_customer
	USING btree
	(record_owner_id, customer_code);

