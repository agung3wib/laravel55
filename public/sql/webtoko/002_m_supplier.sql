CREATE SEQUENCE m_supplier_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE m_supplier (
	supplier_id                              bigint NOT NULL DEFAULT nextval('m_supplier_seq'),
	record_owner_id                          bigint,
	supplier_code                            character varying(50),
	supplier_name                            character varying(100),
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	version                                  bigint,
	active                                   character varying(1),
	active_datetime                          character varying(14),
	non_active_datetime                      character varying(14),
	address1                                 character varying(100),
	address2                                 character varying(100),
	province_id                              bigint,
	city_id                                  bigint,
	industry_type                            character varying(50),
	flg_system                               character varying(1),
	flg_integrate                            character varying(1),
	CONSTRAINT m_supplier_pkey PRIMARY KEY(supplier_id)
);

CREATE UNIQUE INDEX idx_m_supplier_01
	ON m_supplier
	USING btree
	(record_owner_id, supplier_code);

