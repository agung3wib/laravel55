CREATE SEQUENCE m_preorder_product_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE m_preorder_product (
	preorder_product_id                      bigint NOT NULL DEFAULT nextval('m_preorder_product_seq'),
	valid_until_date                         character varying(8),
	curr_code_supplier_price                 character varying(3),
	supplier_code                            character varying(50),
	supplier_product_code                    character varying(50),
	product_code                             character varying(50),
	min_qty                                  bigint,
	max_qty                                  bigint,
	supplier_price                           numeric,
	curr_code_estimate_price                 character varying(3),
	estimate_price                           numeric,
	CONSTRAINT m_preorder_product_pkey PRIMARY KEY(preorder_product_id)
);

