CREATE SEQUENCE pu_receive_goods_item_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE pu_receive_goods_item (
	receive_goods_item_id                    bigint NOT NULL DEFAULT nextval('pu_receive_goods_item_seq'),
	receive_goods_id                         bigint,
	ref_item_id                              bigint,
	product_id                               bigint,
	qty_receive                              bigint,
	version                                  bigint,
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT pu_receive_goods_item_pkey PRIMARY KEY(receive_goods_item_id)
);

