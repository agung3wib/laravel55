CREATE SEQUENCE m_cashbank_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE m_cashbank (
	cashbank_id                              bigint NOT NULL DEFAULT nextval('m_cashbank_seq'),
	record_owner_id                          bigint,
	cashbank_code                            character varying(15),
	cashbank_name                            character varying(50),
	curr_code                                character varying(3),
	remark                                   text,
	version                                  bigint,
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	CONSTRAINT m_cashbank_pkey PRIMARY KEY(cashbank_id)
);

CREATE UNIQUE INDEX idx_m_cashbank_01
	ON m_cashbank
	USING btree
	(record_owner_id, cashbank_code);

