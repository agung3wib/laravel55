CREATE SEQUENCE m_province_seq
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 10
	CACHE 1;

CREATE TABLE m_province (
	province_id                              bigint NOT NULL DEFAULT nextval('m_province_seq'),
	province_name                            bigint,
	CONSTRAINT m_province_pkey PRIMARY KEY(province_id)
);

