CREATE TABLE if_in_m_product_ctgr (
	session_uuid                             character varying(100),
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	product_ctgr_uuid                        character varying(50),
	product_ctgr_code                        character varying(50),
	product_ctgr_parent_uuid                 character varying(50),
	product_ctgr_name                        character varying(100),
	active                                   character varying(1),
	flg_process                              character varying(1)
);

