CREATE TABLE if_in_m_product_supp_info (
	session_uuid                             character varying(100),
	create_datetime                          character varying(14),
	create_username                          character varying(50),
	update_datetime                          character varying(14),
	update_username                          character varying(50),
	supplier_id                              bigint,
	supplier_product_code                    character varying(50),
	supplier_product_name                    character varying(100),
	supplier_price                           character varying(50),
	flg_process                              character varying(1),
	active_date                              character varying(8)
);

