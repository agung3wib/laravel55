<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class ProductSuppInfo extends Model
{
   protected $table      = "m_product_supp_info";
   protected $primaryKey = "m_product_supp_info_id";

}
