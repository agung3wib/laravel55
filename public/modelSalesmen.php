<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class Salesmen extends Model
{
   protected $table      = "m_salesmen";
   protected $primaryKey = "salesmen_id";

}
