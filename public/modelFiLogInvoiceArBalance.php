<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class FiLogInvoiceArBalance extends Model
{
   protected $table      = "fi_log_invoice_ar_balance";
   protected $primaryKey = "log_invoice_ar_balance_id";

}
