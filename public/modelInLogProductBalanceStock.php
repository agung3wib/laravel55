<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class InLogProductBalanceStock extends Model
{
   protected $table      = "in_log_product_balance_stock";
   protected $primaryKey = "log_product_balance_stock_id";

}
