<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class SellPriceLevel extends Model
{
   protected $table      = "m_sell_price_level";
   protected $primaryKey = "sell_price_level_id";

}
