<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class PuReceiveGoodsItem extends Model
{
   protected $table      = "pu_receive_goods_item";
   protected $primaryKey = "receive_goods_item_id";

}
