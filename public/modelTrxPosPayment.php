<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class TrxPosPayment extends Model
{
   protected $table      = "trx_pos_payment";
   protected $primaryKey = "pos_payment_id";

}
