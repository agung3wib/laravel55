<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class TrxPos extends Model
{
   protected $table      = "trx_pos";
   protected $primaryKey = "pos_id";

}
