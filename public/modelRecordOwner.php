<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class RecordOwner extends Model
{
   protected $table      = "m_record_owner";
   protected $primaryKey = "record_owner_id";

}
