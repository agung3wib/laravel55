<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class PuPoItem extends Model
{
   protected $table      = "pu_po_item";
   protected $primaryKey = "po_item_id";

}
