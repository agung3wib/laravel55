<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class FiInvoiceAr extends Model
{
   protected $table      = "fi_invoice_ar";
   protected $primaryKey = "invoice_ar_id";

}
