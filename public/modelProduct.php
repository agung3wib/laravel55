<?php

namespace Sts\WebToko\Model;

use Illuminate\Database\Eloquent\Model;

/**
* @author  Agung
*
*
*/

class Product extends Model
{
   protected $table      = "m_product";
   protected $primaryKey = "product_id";

}
