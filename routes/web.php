<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  Excel::load('Sample2.xlsx', function($reader) {

      // Getting all results

      // ->all() is a wrapper for ->get() and will work the same
      $results = $reader->all();
      $tablein = "";
      $data = array();
      $pk = array();
      $uk = array();
      $script = "";
      $squence_prefix = "";
      $i = 0;
      $h = 0;
      foreach ($results as $k)
      {
        if($k['namafield']=="")
          continue;

        if($k['namatable'] != "")
        {
          if($i>0)
          {
              if(isset($pk[$tablein]))
              {
                $data[$tablein][] = "\tCONSTRAINT ".$tablein."_pkey PRIMARY KEY(".implode(", ", $pk[$tablein]).")";
              }
              $script .= implode(",\n", $data[$tablein]);
              $script .= "\n);\n\n";

              if(isset($uk[$tablein]))
              {
                $index = "";
                foreach ($uk[$tablein] as $x => $y) {
                  $index .= "CREATE UNIQUE INDEX idx_".$tablein."_".zero_magic(str_replace("uk", "", strtolower($x)),2)."\n\tON ".$tablein."\n\tUSING btree\n\t(".implode(", ", $y).");\n\n";
                }

                $script .= $index;

              }

              $class = (substr($tablein,0,2)=='m_')? underScoreToUcWords(substr($tablein,2)) : underScoreToUcWords($tablein);
              $primary = "";
              if(isset($pk[$tablein]))
              {
                  $arpk = $pk[$tablein];
                  if(count($arpk)>1){
                    $primary = "protected ".'$'."primaryKey = [ \"".implode("\", \"", $arpk)."\" ];";
                  } else {
                    $primary = "protected ".'$'."primaryKey = \"".$arpk[0]."\";";
                  }
              }

              $content = file_get_contents(base_path().'/public/models.txt');
              $content = str_replace('{class}', $class , $content);
              $content = str_replace('{pkey}', $primary , $content);
              $content = str_replace('{table}', $tablein , $content);

              $filemodel = $class.".php";
              file_put_contents("./model/".$filemodel, $content);

              if(substr($tablein,0,2)=='if')
              {
                $file = zero_magic(++$h, 3)."_".$tablein.".sql";
                file_put_contents("./sql/staging/".$file, $script);
              }

              $file = zero_magic($i, 3)."_".$tablein.".sql";
              file_put_contents("./sql/webtoko/".$file, $script);
              $script = "";
          }

          if($k['sequence'] == 'V')
          {
            $seq = "CREATE SEQUENCE ".$k['namatable']."_seq\n\tINCREMENT 1\n\tMINVALUE 1\n\tMAXVALUE 9223372036854775807\n\tSTART 10\n\tCACHE 1;\n\n";
            $script .= $seq;
            $fieldseq = " NOT NULL DEFAULT nextval('".$k['namatable']."_seq')";
          }


          $tablein = $k['namatable'];

          $prefix_schm = "";
          if(sub_str($tablein, 0, 2)=='m_')
          {
            $prefix_schm = 'mst.';
          }
          $script .= "CREATE TABLE ".$prefix_schm.$tablein." (\n";
          $i++;
        }

        if($k['sequence'] != 'V')
          $fieldseq = "";

        $field = "\t".$k['namafield'];
        $sisa = 40 - strlen($k['namafield']);

        for ($j=0; $j <=$sisa ; $j++) {
          $field .= " ";
        }

        $field .= $k['type'].$fieldseq;

        // if($k['default'] != '')
        // {
        //   $field .= " DEFAULT ".$k['default'];
        // }

        $data[$tablein][] = $field;
        if($k['pk'] == 'V')
        {
          $pk[$tablein][] = $k['namafield'];
        }

        if($k['uk'] != '')
        {
          $uk[$tablein][$k['uk']][] = $k['namafield'];
        }

      }


        if(isset($pk[$tablein]))
        {
          $data[$tablein][] = "\tCONSTRAINT ".$tablein."_pkey PRIMARY KEY(".implode(", ", $pk[$tablein]).")";
        }
        // var_dump($data[$tablein]);
        // exit();
        $script .= implode(",\n", $data[$tablein]);

        $script .= "\n);\n\n";

        if(isset($uk[$tablein]))
        {
          $index = "";
          foreach ($uk[$tablein] as $x => $y) {
            $index .= "CREATE UNIQUE INDEX idx_".$tablein."_".zero_magic(str_replace("uk", "", strtolower($x)),2)."\n\tON ".$tablein."\n\tUSING btree\n\t(".implode(", ", $y).");\n\n";
          }

          $script .= $index;

        }

        $class = (substr($tablein,0,2)=='m_')? underScoreToUcWords(substr($tablein,2)) : underScoreToUcWords($tablein);
        $primary = "";
        if(isset($pk[$tablein]))
        {
            $arpk = $pk[$tablein];
            if(count($arpk)>1){
              $primary = "protected ".'$'."primaryKey = [ \"".implode("\", \"", $arpk)."\" ];";
            } else {
              $primary = "protected ".'$'."primaryKey = \"".$arpk[0]."\";";
            }
        }

        $content = file_get_contents(base_path().'/public/models.txt');
        $content = str_replace('{class}', $class , $content);
        $content = str_replace('{pkey}', $primary , $content);
        $content = str_replace('{table}', $tablein , $content);

        $filemodel = $class.".php";
        file_put_contents("./model/".$filemodel, $content);

        if(substr($tablein,0,2)=='if')
        {
          $file = zero_magic(++$h, 3)."_".$tablein.".sql";
          file_put_contents("./sql/staging/".$file, $script);
        }

        $file = zero_magic($i, 3)."_".$tablein.".sql";
        file_put_contents("./sql/webtoko/".$file, $script);

      // echo $script;


  });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
